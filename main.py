try:
    import pandas
    from prettytable import PrettyTable
except ModuleNotFoundError:
    import os
    print("You did not install the required dependencies, let me do that for you")
    os.system('pip install -r requirements.txt')

from datetime import datetime
from config import *
GRADES = GRADES[::-1]

def time_formatter(given_hour, given_minute):
    if given_hour == 0:
        given_hour = 12
        meridian = " AM"
    elif given_hour == 12:
        meridian = " PM"
    elif given_hour > 12:
        given_hour -= 12
        meridian = " PM"
    else:
        meridian = " AM"
    return f"{given_hour}:{given_minute}{meridian}"

def give_grade(got, total, grade_list = GRADES):
    grade_id = int(num_range(got,total, valuerange=len(grade_list)))
    while grade_id > (len(grade_list) - 1):
        grade_id -= 1
    return grade_list[grade_id]


def num_range(got, total, valuerange=100):
    global ROUND_PRECISION
    return round((got / total) * valuerange, ROUND_PRECISION)


filename = input("Enter the input filename without extension, should be an excel file(xlsx) and inside input folder: ")

try:
    excel = pandas.read_excel(f"{INPUT_FOLDER}/{filename}.xlsx")
except FileNotFoundError:
    print(f"Input file '{filename}' not found, make sure you type filename with its case in mind without extension.")
    exit(-1)
heads = excel.columns.to_list()
date_today = f"{datetime.now().year}_{datetime.now().month}_{datetime.now().day}"
time_now = time_formatter(datetime.now().hour, datetime.now().minute)
file = open(f"{OUTPUT_FOLDER}/{SUBJECT_NAME}_{date_today}.txt", "w")
file.writelines(
    f"Report generated date: {date_today.replace('_','/')} AD, time: {time_now}\nSubject: {SUBJECT_NAME}\nPass Marks: {PASS_MARKS}\nFull Marks: {FULL_MARKS}\n---"
    f"--------------------------------")
failed_tables = []
passed_tables = []
index = 0
for head in heads:
    index += 1
    failed_table = PrettyTable()
    passed_table = PrettyTable()
    failed_table.field_names = ['Roll No', 'Obtained Marks', 'Required marks to pass','% scored', 'GPA', 'Grade']
    passed_table.field_names = ['Roll No', 'Obtained Marks', 'Passed by marks', '% scored', 'GPA', 'Grade']
    series = excel[head].dropna()
    size = series.size
    sd = series.std()
    median = series.median()
    lowest = series.min()
    lowest_scorer = []
    highest = series.max()
    highest_scorer = []
    average = series.mean()
    total_sum = series.sum()
    values = series.to_list()
    number_of_failed = 0
    number_of_passed = 0
    total_marks_possible = FULL_MARKS * len(values)
    for value in range(len(values)):
        if values[value] < PASS_MARKS:
            number_of_failed += 1
            failed_table.add_row([value + 1, values[value], PASS_MARKS - values[value], f"{num_range(values[value], FULL_MARKS)}%", num_range(values[value], FULL_MARKS, 4), give_grade(values[value], FULL_MARKS,GRADES)])
        elif values[value] >= PASS_MARKS:
            number_of_passed += 1
            passed_table.add_row(
                [value + 1, values[value], values[value] - PASS_MARKS, f"{num_range(values[value], FULL_MARKS)}%", num_range(values[value], FULL_MARKS, 4), give_grade(values[value], FULL_MARKS,GRADES)])
        if values[value] == lowest:
            lowest_scorer.append(value + 1)
        if values[value] == highest:
            highest_scorer.append(value + 1)
    failed_tables.append(failed_table)
    passed_tables.append(passed_table)
    to_be_printed = f"""
{head}::

Total Number of entries = {size}
Lowest marks = {lowest} by roll number(s): {lowest_scorer}
highest marks = {highest} by roll number(s): {highest_scorer}
Total marks by all students = {total_sum} / {total_marks_possible} (if everyone in class scored full marks)
Number of failed students = {number_of_failed} ({num_range(number_of_failed, size)})%
Number of passed students = {number_of_passed} ({num_range(number_of_passed, size)})%
-----------------------------------
Statistical analysis:

Average(Mean) = {average}  [Aim to make this number higher]
Standard Deviation = {sd}  [Aim to make this number lower]
-----------------------------------
Table containing failed students:
{failed_tables[index - 1]}
-----------------------------------
Table containing passed students:
{passed_tables[index - 1]}
-----------------------------------
-----------------------------------
"""
    file.writelines(to_be_printed)
file.close()
print(f'File named "{SUBJECT_NAME}_{date_today}.txt" has been generated in output folder')
print("\n")
print(f"Pass marks is set as {PASS_MARKS}, full marks as {FULL_MARKS} and subject as {SUBJECT_NAME}")
print("If it is not desired, please change that in config.py file.")
