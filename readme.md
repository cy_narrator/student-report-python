I made this project when I was practising Python. Though the code may not look very nice right now(cleaning work is still
pending), it nonetheless does what it is supposed to do so I thought it will still be of help to somebody if they find it.

# Setup Instructions
Step 0: Make sure Python works along with pip package manager

Step 1: Download Zip or git clone this URL
    ```git clone https://gitlab.com/cy_narrator/student-report-python.git```

Step 2: Edit the config.py file to change things like subject name, pass marks and full marks

Step 3: Install the required dependencies:
    ```pip install -r requirements.txt```

Step 4: Create 'input' and 'output' folder from where main.py is

Step 5: Run main.py python script
    ```python main.py```

Step 6: Enjoy