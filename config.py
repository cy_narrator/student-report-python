PASS_MARKS = 40
FULL_MARKS = 100
SUBJECT_NAME = "My_Subject"
INPUT_FOLDER = "input"
OUTPUT_FOLDER = "output"
GRADES = ['A+', 'A', 'B+', 'B', 'C+', 'C', 'D+', 'D', 'F', 'Not Graded']
MAX_GPA = 4.0
ROUND_PRECISION = 2